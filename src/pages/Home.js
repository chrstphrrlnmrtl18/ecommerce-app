import {Row, Carousel} from "react-bootstrap";
import dog1 from '../images/dog1.jpg';
import dog2 from '../images/dog2.jpg';
import dog3 from '../images/dog3.jpg';
import ProductCard from "../components/ProductCard"


import { useEffect, useState, useContext } from "react";

import {motion} from "framer-motion";

import AboutUs from './AboutUs';
import MissionVision from './MissionVision';
import UserContext from "../UserContext";
export default function Home(){
  const {user} = useContext(UserContext)
  const [products, setProducts] = useState([]);
  useEffect(()=>{

    

    fetch(`${process.env.REACT_APP_API_URL}products/activeproducts`)
    .then(response => response.json())
    .then(data => {
       
        let start = data.length - 3;
        
        setProducts(data.slice(start, data.length).map(product=>{
            return (
                <ProductCard key={product._id} productProp={product}/>
            )
        }))
    })
},[])
    
    return(
      <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
        
        <h1 className="text-center mt-3" id= "welcome"><strong>Welcome to Phoebe's shop</strong></h1>
        <p className = "text-center" id = "wel-cap"><strong>where you can buy all your dog needs. . . </strong></p>
           
      <Row md={2} id = "homepage" className = "mt-3 mb-3">
           <Carousel variant="fade" className = "mt-3" id = 'carousel'>
      <Carousel.Item>
        <img
          className="d-block w-50 mx-auto img-fluid"
          src={dog1}
          alt="First slide"
          width = '50%'
          height= '50%'
        />
        <Carousel.Caption>
          <h5>First slide label</h5>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-50 mx-auto img-fluid"
          src={dog2}
          alt="Second slide"
          width = '50%'
          height= '50%'
        />
        <Carousel.Caption>
          <h5>Second slide label</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-50 mx-auto img-fluid"
          src={dog3}
          alt="Third slide"
          width = '50'
          height= '50'
        />
        <Carousel.Caption>
          <h5>Third slide label</h5>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
    
        <AboutUs/>
        <MissionVision/>
           </Row> 
           
   {
        (user.isAdmin)
        ?
          <></>
        :
       <>
       <h3 className="text-center mt-5" id = 'featProd'><strong>Recently added products</strong></h3>
        <Row md={3} className="g-3 mb-5">
                 {products}
        </Row>
       </>   
   }
   
      
       
        </motion.div>
    )
}