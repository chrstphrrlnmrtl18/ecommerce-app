import {Accordion} from 'react-bootstrap';
import {useContext, useState} from 'react';
import UserContext from '../UserContext';

import {motion} from "framer-motion";

export default function UserOrder(){
    const{ user } = useContext(UserContext);
    
    const [userOrder, setUserOrder] = useState([])
   
        fetch(`${process.env.REACT_APP_API_URL}users/orders`, {
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setUserOrder(data.map(order => {
                  
                return(
                      <Accordion.Item eventKey={order._id} id = 'accord'>
                      <Accordion.Header >Transaction no: {order._id} on {order.dateAdded}</Accordion.Header>
                      <Accordion.Body>
                       <h3><strong>{order.product[0].productName}</strong></h3>
                        <ol>{order.product[0].quantity} pieces</ol>
                        <ol>{order.totalAmount} pesos</ol>
                      </Accordion.Body>
                    </Accordion.Item>
                      )
                }))})
    return(
    <motion.div initial ={{opacity: 0}} animate={{opacity: 1}} exit = { {opacity: 0}}>
    <h1 className='text-center mt-5 '>Here are your order history </h1>
    <h1 className='text-center mt-5 '>Sir/Ma'am <strong> {user.name} {user.lastName}</strong></h1>
    <Accordion className='mt-5 mb-5'>
      {userOrder}
    </Accordion>
    <h1 className='text-center mt-5 '>End of orders</h1>

    </motion.div>
    )
}